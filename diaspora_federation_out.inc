<?php
include_once('diaspora_utils.inc');

/**
 * Send a request Notification
 *
 * @param object $sender_account the originator of this message
 * @param object $target_account the target of this message
 */
function diaspora_send_share_notification($sender_account, $target_account) {
  
  $sender_handle = $sender_account->name . '@home.rurri.com';
  $recipient_handle = $target_account->name;
  
 $xml = <<<EOD
<XML>
  <post>
    <request>
      <sender_handle>$sender_handle</sender_handle>
      <recipient_handle>$recipient_handle</recipient_handle>
    </request>
  </post>
</XML>
EOD;

  diaspora_send_message($xml, $sender_account, $target_account);
}

/**
 * Send a request retraction
 *
 * @param object $sender_account the originator of this message
 * @param object $target_account the target of this message
 */
function diaspora_send_share_retraction($sender_account, $target_account) {
  $sender_handle = $sender_account->name . '@home.rurri.com';
  
  $diaspora_user = _diaspora_user_by_uid($sender_account->uid);
 $xml = <<<EOD
<XML>
  <post>
    <retraction>
      <post_guid>$diaspora_user->uuid</post_guid>
      <diaspora_handle>$sender_handle</diaspora_handle>
      <type>Person</type>
    </retraction>
  </post>
</XML>
EOD;

  diaspora_send_message($xml, $sender_account, $target_account);
}

/**
 * Send a status message
 *
 * @todo Handle both public and non-public messages.  Currently hardcoded to non-public.
 *
 * @param object $sender_account the originator of this message
 * @param object $target_account the target of this message
 * @param string $message the contents of the message
 * @param string $guid The global unique id of the message
 * @param timestamp $created_time The time the message was posted
 */
function diaspora_send_status_message($sender_account, $target_account, $message, $guid, $created_time) {
  $sender_handle = $sender_account->name . '@home.rurri.com';
  $diaspora_user = _diaspora_user_by_uid($sender_account->uid);
  $created = date('Y-m-d H:i:s T', $created_time);
 $xml = <<<EOD
<XML>
  <post>
    <status_message>
      <raw_message>$message</raw_message>
      <guid>$guid</guid>
      <diaspora_handle>$sender_handle</diaspora_handle>
      <public>false</public>
      <created_at>$created</created_at>
    </status_message>
  </post>
</XML>
EOD;

  diaspora_send_message($xml, $sender_account, $target_account);
}


/**
 * Send a public status message
 *
 * @param object $sender_account the originator of this message
 * @param string $target_pod_url the url of the target pod
 * @param string $message the contents of the message
 * @param string $guid The global unique id of the message
 * @param timestamp $created_time The time the message was posted
 */
function diaspora_send_public_status_message($sender_account, $target_pod_url, $message, $guid, $created_time) {
  $sender_handle = $sender_account->name . '@home.rurri.com';
  $diaspora_user = _diaspora_user_by_uid($sender_account->uid);
  $created = date('Y-m-d H:i:s T', $created_time);
  
 $xml = <<<EOD
 <XML>
  <post>
    <status_message>
      <raw_message>$message</raw_message>
      <guid>$guid</guid>
      <diaspora_handle>$sender_handle</diaspora_handle>
      <public>true</public>
      <created_at>$created</created_at>
    </status_message>
  </post>
</XML>
EOD;

  diaspora_send_public_message($xml, $sender_account, $target_pod_url);
}



function diaspora_send_like($sender_account, $target_account, $target_type, $like_guid, $parent_guid) {
  $sender_handle = $sender_account->name . '@home.rurri.com';
  $diaspora_user = _diaspora_user_by_uid($sender_account->uid);
  $status_author = _diaspora_user_by_uid($status_author_uid);
  

  $base_string = "$like_guid;$target_type;$parent_guid;true;$sender_handle";
  $author_signature  = base64_encode(_diaspora_sign_data($diaspora_user->private_key, $base_string));
  $parent_author_sig = '';
  if ($status_author) {
    $parent_author_signature  = '<parent_author_signature>' . base64_encode(_diaspora_sign_data($status_author->private_key, $base_string)) . '</parent_author_signature>';
  }
  _diaspora_user_by_uid($sender_account->uid);

 $xml = <<<EOD
<XML>
  <post>
    <like>
      <guid>$like_guid</guid>
      <target_type>$target_type</target_type>
      <parent_guid>$parent_guid</parent_guid>
      <author_signature>$parent_author_signature</author_signature>
      <positive>true</positive>
      <diaspora_handle>$sender_handle</diaspora_handle>
    </like>
  </post>
</XML>
EOD;

  diaspora_send_message($xml, $sender_account, $target_account);
}

function diaspora_send_public_like($sender_account, $target_account, $target_type, $like_guid, $parent_guid) {
  $sender_handle = $sender_account->name . '@home.rurri.com';
  $diaspora_user = _diaspora_user_by_uid($sender_account->uid);
  $created = date('Y-m-d H:i:s T', $created_time);
  
 $xml = <<<EOD
<XML>
  <post>
    <like>
      <guid>$like_guid</guid>
      <target_type>$target_type</target_type>
      <parent_guid>$parent_guid</parent_guid>
      <author_signature>$parent_author_signature</author_signature>
      <positive>true</positive>
      <diaspora_handle>$sender_handle</diaspora_handle>
    </like>
  </post>
</XML>
EOD;

  diaspora_send_public_message($xml, $sender_account, $target_pod_url);
}

/**
 * Send a status message
 *
 * @param object $sender_account the originator of this comment
 * @param object $target_account the target of this message
 * @param string $message the contents of the comment
 * @param string $guid The global unique id of the comment
 * @param string $status_guid The global unique id of the status this comment is attached to
 * @param boolean $public Whether the message is public or private
 */
function diaspora_send_status_comment($sender_account, $target_account, $status_author_uid, $message, $guid, $status_guid) {
  $sender_handle = $sender_account->name . '@home.rurri.com';
  $diaspora_user = _diaspora_user_by_uid($sender_account->uid);
  $status_author = _diaspora_user_by_uid($status_author_uid);
  

  $base_string = "$guid;$status_guid;$message;$sender_handle";
  $author_signature  = base64_encode(_diaspora_sign_data($diaspora_user->private_key, $base_string));
  $parent_author_sig = '';
  if ($status_author) {
    $parent_author_signature  = '<parent_author_signature>' . base64_encode(_diaspora_sign_data($status_author->private_key, $base_string)) . '</parent_author_signature>';
  }
  _diaspora_user_by_uid($sender_account->uid);

 $xml = <<<EOD
<XML>
  <post>
    <comment>
      <guid>$guid</guid>
      <parent_guid>$status_guid</parent_guid>
       $parent_author_signature
      <author_signature>$author_signature</author_signature>
      <text>$message</text>
      <diaspora_handle>$sender_handle</diaspora_handle>
    </comment>
  </post>
</XML>
EOD;

  diaspora_send_message($xml, $sender_account, $target_account);
}




/**
 * Send a status message
 *
 * @todo Handle both public and non-public messages.  Currently hardcoded to non-public.
 *
 * @param object $sender_account the originator of this message
 * @param object $target_account the target of this message
 * @param string $message the contents of the message
 * @param string $guid The global unique id of the message
 * @param timestamp $created_time The time the message was posted
 * @param boolean $public Whether the message is public or private
 */
function diaspora_retract_status_message($sender_account, $target_account, $guid, $created_time, $public) {
  $sender_handle = $sender_account->name . '@home.rurri.com';
  $isPublic = $public ? 'true' : 'false';
  $diaspora_user = _diaspora_user_by_uid($sender_account->uid);
  $created = date('Y-m-d H:i:s T', $created_time);
 $xml = <<<EOD
<XML>
  <post>
    <relayable_retraction>
      <parent_author_signature>ONdFbm6DfLxzu1mLJ1eTqMKVfq4zGfZQjKvPNIbKTr2EqkDQhYnlUeV1rMtUDlK+W4S44VDgGaBsRxJzUE2CBT7es7Ntzf5CB4qBdvI+bENupkDqqfwYWfzNp4UO22rnUZyctv7wP9JS+jkVeDCbnjedtG2Pw/Z4MJ+2fU7Gaklc3SO+mlUi0p0OX1J0wsVqFAalOr2YCmNai7bZwFF7vY5MmVXciOhsk3JV7r0Xw6OJ0IqwjL6D0JDCPdC5mJUtgtyA58H4sFzrtTHGIcaksm6+l/LejXGcLVaPZkkw6Kx4W8JuRC+d0S9Ybu6mFCmjS+Rfq14l89mji/0tDpDH3+4TxNyeIxWSXctFFDm9ABJPtKErW2KkdUbBjX8oVFRFNW1zeKmRUonhJ1ho02aXVcDnwlzGeYb7npe63Zqgzqv8dogT/wMNmQwvwrEMVUOBJbGXzcVAsOSWZeLLMFQXkkt3BjRCM0vOlTHS9pOCavyjzrLIRH2reDomwG+ycZSVvSx/Ywwc8ugKnCCGjJW9spnpjUncWdlgX10VlAboc1CF1jGB6Pryx9uDxsM5iB0YktlUPNKyFEfpYeDzjZAjwj7fCz9DY1BejkUXUpSEhZnB37IabMqK6PROC004S0t8dfzHipPTDyL/lhAJODZSHj49VjmcfBzQ44GPR+bZdZw=</parent_author_signature>
      <target_guid>$guid</target_guid>
      <target_type>Comment</target_type>
      <sender_handle>$sender_handle</sender_handle>
      <target_author_signature>ONdFbm6DfLxzu1mLJ1eTqMKVfq4zGfZQjKvPNIbKTr2EqkDQhYnlUeV1rMtUDlK+W4S44VDgGaBsRxJzUE2CBT7es7Ntzf5CB4qBdvI+bENupkDqqfwYWfzNp4UO22rnUZyctv7wP9JS+jkVeDCbnjedtG2Pw/Z4MJ+2fU7Gaklc3SO+mlUi0p0OX1J0wsVqFAalOr2YCmNai7bZwFF7vY5MmVXciOhsk3JV7r0Xw6OJ0IqwjL6D0JDCPdC5mJUtgtyA58H4sFzrtTHGIcaksm6+l/LejXGcLVaPZkkw6Kx4W8JuRC+d0S9Ybu6mFCmjS+Rfq14l89mji/0tDpDH3+4TxNyeIxWSXctFFDm9ABJPtKErW2KkdUbBjX8oVFRFNW1zeKmRUonhJ1ho02aXVcDnwlzGeYb7npe63Zqgzqv8dogT/wMNmQwvwrEMVUOBJbGXzcVAsOSWZeLLMFQXkkt3BjRCM0vOlTHS9pOCavyjzrLIRH2reDomwG+ycZSVvSx/Ywwc8ugKnCCGjJW9spnpjUncWdlgX10VlAboc1CF1jGB6Pryx9uDxsM5iB0YktlUPNKyFEfpYeDzjZAjwj7fCz9DY1BejkUXUpSEhZnB37IabMqK6PROC004S0t8dfzHipPTDyL/lhAJODZSHj49VjmcfBzQ44GPR+bZdZw=</target_author_signature>
    </relayable_retraction>
  </post>
</XML>
EOD;

  diaspora_send_message($xml, $sender_account, $target_account);
}



/**
 * Send a status Notification
 * @todo Make site name dynamic
 */
/*
function diaspora_send_status_notifications($node) {
  dsm($node);
  
  $author = user_load($node->uid);
  
  $select = db_select('diaspora_follower');
  $select->fields('diaspora_follower');
  $select->condition('target_uid', $node->uid);
  $follower_ids = $select->execute()->fetchCol(0);

  if (!$follower_ids) {
    return;
  }
  $select = db_select('diaspora_external_user');
  $select->fields('diaspora_external_user');
  $select->condition('uid', $follower_ids, 'IN');
  $followers = $select->execute()->fetchAll();
  
  $select = db_select('diaspora_user');
  $select->fields('diaspora_user');
  $select->condition('uid', $node->uid);
  $diaspora_user = $select->execute()->fetchAll();
  if ($diaspora_user) {
    $diaspora_user = $diaspora_user[0];
    
    $rawMessage = $node->body[$node->language][0]['value'];
    $created = date('Y-m-d H:i:s T', $node->created);// . ' UTC';
    $guid = uniqid();
    $xml = <<<EOD
  <XML>
  <post>
    <status_message>
      <raw_message>$rawMessage</raw_message>
      <guid>$guid</guid>
      <diaspora_handle>$author->name@home.rurri.com</diaspora_handle>
      <public>false</public>
      <created_at>$created</created_at>
    </status_message>
  </post>
</XML>
EOD;

    dsm($xml);
    foreach($followers as $follower) {
     
      diaspora_send_message()

    }
  } else {
    
    dsm('error');

  }
  
}
*/

/**
 * Sends an XML Message to the target user
 *
 * @param string $message The xml of the message to send
 * @param object $author The originator of the message
 * @param object $target The target of the message
 *
 * @todo make sitename dynamic
 */
function diaspora_send_message($message, $author, $target) {
  
  dsm($message);
  $target_diaspora_user = _diaspora_external_user_by_uid($target->uid);
  $source_diaspora_user = _diaspora_user_by_uid($author->uid);
  $magic_envelope = _diaspora_get_private_message($message, $author, $target_diaspora_user, $source_diaspora_user->private_key);
  
  $url = "{$target_diaspora_user->seed_location}receive/users/$target_diaspora_user->uuid/";
  $encoded_magic_envelope = urlencode(urlencode($magic_envelope));
  
  dsm($url);
  dsm($magic_envelope);
 
  
  $url_headers = array(
    'Content-Type' => 'application/x-www-form-urlencoded',
    'User-Agent' => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.9) Gecko/20071025 Firefox/2.0.0.9'
  );

  $result = drupal_http_request($url, array('headers' => $url_headers,
                                            'data' => "xml=$encoded_magic_envelope",
                                            'method' => 'POST'));
  dsm($result);
}

function diaspora_send_public_message($message, $author, $target_uri) {
  dsm('test1');
  dsm($message);
  dsm('test2');
  $source_diaspora_user = _diaspora_user_by_uid($author->uid);
  $magic_envelope = _diaspora_get_public_message($message, $author, $source_diaspora_user->private_key);
  $url = "{$target_uri}receive/public";
  $encoded_magic_envelope = urlencode(urlencode($magic_envelope));
  
  dsm($url);
  dsm($magic_envelope);
 
  
  $url_headers = array(
    'Content-Type' => 'application/x-www-form-urlencoded',
    'User-Agent' => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.9) Gecko/20071025 Firefox/2.0.0.9'
  );


  $options = array('headers' => $url_headers,
                    'data' => "xml=$encoded_magic_envelope",
                  'method' => 'POST');
  $result = drupal_http_request($url, $options);
  dsm($result);
}


function _diaspora_sign_data($private_key, $data) {
  $private_key_res = openssl_pkey_get_private($private_key);
  $signature = '';
  openssl_sign($data, $signature, $private_key_res, 'sha256');
  return $signature;
}

function _diaspora_get_private_message($message, $author, $external_target, $private_key) {
  

  $inner_key = openssl_random_pseudo_bytes(32);
  $inner_iv =  openssl_random_pseudo_bytes(16);
  $encoded_inner_key = base64_encode($inner_key);
  $encoded_inner_iv = base64_encode($inner_iv);
  
  
  $outer_key = openssl_random_pseudo_bytes(32);
  $outer_iv  =  openssl_random_pseudo_bytes(16);
  $encoded_outer_key = base64_encode($outer_key);
  $encoded_outer_iv = base64_encode($outer_iv);
  $base64_public_key = strtr($external_target->public_key, '-_', '+/');
  $plain_text_public_key = base64_decode($base64_public_key);
  if (strpos($plain_text_public_key, '-----BEGIN RSA PUBLIC KEY-----') === 0) {
    $formatted_key = diaspora_openssh2pem($plain_text_public_key);
  } else {
    $formatted_key = $plain_text_public_key;
  }
  

  $public_key_res = openssl_pkey_get_public($formatted_key);
  
$header = <<<EOD
  <decrypted_header>
    <iv>$encoded_inner_iv</iv>
    <aes_key>$encoded_inner_key</aes_key>
    <author_id>$author->name@home.rurri.com</author_id>
  </decrypted_header>
EOD;

  $bae64_encoded_ciphertext = openssl_encrypt ($header, 'aes-256-cbc', $outer_key, FALSE, $outer_iv);
  $outer_aes_key_bundle = json_encode(array('iv' => $encoded_outer_iv, 'key' => $encoded_outer_key));
  
  $encrypted_outer_aes_key_bundle = '';
  openssl_public_encrypt($outer_aes_key_bundle, $encrypted_outer_aes_key_bundle, $public_key_res);
  $encrypted_header_json_object = json_encode(array('aes_key' => base64_encode($encrypted_outer_aes_key_bundle), 'ciphertext' => $bae64_encoded_ciphertext));
  
  $encrypted_header = '<encrypted_header>' . base64_encode($encrypted_header_json_object) . '</encrypted_header>';
  $base64_encoded_encrypted_payload = '';
  
  $base64_encoded_encrypted_payload = openssl_encrypt ($message, 'aes-256-cbc', $inner_key, FALSE, $inner_iv);
  $base64url_encoded_base64_encoded_encrypted_payload = diaspora_base64url_encode($base64_encoded_encrypted_payload);
  
  $data_type = 'application/xml';
  
  $base_string = $base64url_encoded_base64_encoded_encrypted_payload;
  $base_string .= ".YXBwbGljYXRpb24veG1s";
  $base_string .= ".YmFzZTY0dXJs";
  $base_string .= ".UlNBLVNIQTI1Ng==";
  $private_key_res = openssl_pkey_get_private($private_key);
  $sig = '';
  openssl_sign($base_string, $sig, $private_key_res, 'sha256');
  $encoded_sig = diaspora_base64url_encode($sig);

  $magic_envelope = <<<EOD
<?xml version='1.0' encoding='UTF-8'?>
<diaspora xmlns="https://joindiaspora.com/protocol" xmlns:me="http://salmon-protocol.org/ns/magic-env">
  $encrypted_header
  <me:env>
    <me:data type='$data_type'>$base64url_encoded_base64_encoded_encrypted_payload</me:data>
    <me:encoding>base64url</me:encoding>
    <me:alg>RSA-SHA256</me:alg>
    <me:sig>$encoded_sig</me:sig>
  </me:env>
</diaspora>
EOD;

  return $magic_envelope;
}

function _diaspora_get_public_message($message, $author, $private_key) {
  

  $inner_key = openssl_random_pseudo_bytes(32);
  $inner_iv =  openssl_random_pseudo_bytes(16);
  $encoded_inner_key = base64_encode($inner_key);
  $encoded_inner_iv = base64_encode($inner_iv);
  
  
  $outer_key = openssl_random_pseudo_bytes(32);
  $outer_iv  =  openssl_random_pseudo_bytes(16);
  $encoded_outer_key = base64_encode($outer_key);
  $encoded_outer_iv = base64_encode($outer_iv);
  
$header = <<<EOD
  <header>
    <author_id>$author->name@home.rurri.com</author_id>
  </header>
EOD;

 
  $base64_encoded_payload = ($message);
  $base64url_encoded_base64_encoded_payload = diaspora_base64url_encode($base64_encoded_payload);
  
  $data_type = 'application/xml';
  
  $base_string = $base64url_encoded_base64_encoded_payload;
  $base_string .= ".YXBwbGljYXRpb24veG1s";
  $base_string .= ".YmFzZTY0dXJs";
  $base_string .= ".UlNBLVNIQTI1Ng==";
  $private_key_res = openssl_pkey_get_private($private_key);
  $sig = '';
  openssl_sign($base_string, $sig, $private_key_res, 'sha256');
  $encoded_sig = diaspora_base64url_encode($sig);

  $magic_envelope = <<<EOD
<?xml version='1.0' encoding='UTF-8'?>
<diaspora xmlns="https://joindiaspora.com/protocol" xmlns:me="http://salmon-protocol.org/ns/magic-env">
  $header
  <me:env>
    <me:data type='$data_type'>$base64url_encoded_base64_encoded_payload</me:data>
    <me:encoding>base64url</me:encoding>
    <me:alg>RSA-SHA256</me:alg>
    <me:sig>$encoded_sig</me:sig>
  </me:env>
</diaspora>
EOD;

  return $magic_envelope;
}