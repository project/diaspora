<?php

/**
 * This file contains the definition and documentation for all the hooks of the diaspora module
 */

/**
 * An external user has started sharing with a user on the drupal system.  The drupal user does
 * not need to necessarily care or do anything or may not even want to know about it.
 *
 * In general this is a sign that the user may want to share back, and also that the user is most
 * likely in at least seeing the user's public messages from now on even if the user does not
 * decide to explicitely share anything back.
 *
 * This information is generally logged so that future public posts can be sent back.
 *
 * @param object $sender_account The drupal account object of the message sender
 * @param object $target_account The drupal account object of the target user
 * @param string $sender_handle The Diaspora handle of the user initiating the sharing
 * @param DOMDocument $raw_xml The raw XML recieved by the drupal system
 */
function hook_diaspora_share($sender_account, $target_account, $sender_handle, $recipient_handle, $raw_xml) {
  
}

/**
 * An external user has <b>stopped</b> sharing with a user on the drupal system. The drupal user does
 * not need to necessarily care or do anything or may not even want to know about it.
 *
 * In general this is a sign that the user may want to stop shareing back, and also that the user is most
 * likely <b>NOT</b> any longer interested in the user's public messages from now on,
 *
 * This information is generally logged so that future public posts will no longer be sent.
 *
 * @param object $sender_account The drupal account object of the message sender
 * @param object $target_account The drupal account object of the target user
 * @param string $sender_handle The Diaspora handle of the user stopping the sharing
 * @param DOMDocument $raw_xml The raw XML recieved by the drupal system
 */
function hook_diaspora_unshare($sender_account, $target_account, $sender_handle, $raw_xml) {
  
}

/**
 * A new status message notification has been received by the drupal system from a remote diaspora system.
 * The drupal user does not need to necessarily care or do anything or may not even want to know about it.
 *
 * The message may now be shown in searches (if it is public).
 *
 * The message will also show up in any user's streams if those users are following the remote user.
 *
 * @param string $raw_message The contents of the status update
 * @param string $status_guid The GUID of the status update
 * @param string @author_handle The diaspora handle of the author of the status update
 * @param timestamp @created_at The date and time the message was created
 * @param DOMDocument $raw_xml The raw XML recieved by the drupal system
 * @param object $account The drupal account object of the target user
 */
function hook_diaspora_status_message($raw_message, $status_guid, $author_handle, $created_at, $raw_xml, $public = FALSE, $account = NULL) {
  
}

/**
 * A user is attempting to retract a status update already sent.
 *
 * The message should be attempted to be deleted and removed from the system.
 *
 * @param string $status_guid The GUID of the status update
 * @param string @author_handle The diaspora handle of the author of the status update
 * @param DOMDocument $raw_xml The raw XML recieved by the drupal system
 * @param object $account The drupal account object of the target user
 */

function hook_diaspora_status_message_retraction($status_guid, $author_handle, $raw_xml, $account = NULL) {
  
}

/**
 * A new comment on a status message has been received by the drupal system from a remote diaspora system.
 * The drupal user does not need to necessarily care or do anything or may not even want to know about it.
 *
 * The comment should be attached to the original message (if received).
 *
 * @param string $text The contents of the comment
 * @param string $comment_guid The GUID of the comment
 * @param string $status_guid The GUID of the status update the comment is attached to
 * @param string @author_handle The diaspora handle of the author of the comment
 * @param DOMDocument $raw_xml The raw XML recieved by the drupal system
 * @param object $account The drupal account object of the target user (if available)
 */
function hook_diaspora_comment($text, $comment_guid, $status_guid, $author_handle, $raw_xml, $account = NULL) {
  
}

/**
 * A user is attempting to retract a comment already sent.
 *
 * The comment should be attempted to be deleted and removed from the system.
 *
 * @param string $comment_guid The GUID of the comment
 * @param string $status_guid The GUID of the status update the comment is attached to
 * @param string @author_handle The diaspora handle of the author of the status update
 * @param DOMDocument $raw_xml The raw XML recieved by the drupal system
 * @param object $account The drupal account object of the target user
 */
function hook_diaspora_comment_retraction($comment_guid, $status_guid, $author_handle, $raw_xml, $account = NULL) {
  
}

/**
 * A new like has been received by the drupal system from a remote diaspora system.
 * The drupal user does not need to necessarily care or do anything or may not even want to know about it.
 *
 * The like should be attached to the original message or comment (if recieved)
 *
 * @param string $target_type A String description of the type of object being liked (Post/Comment)
 * @param string $like_guid The GUID of the like
 * @param string $parent_guid The GUID of the object being liked
 * @param string @author_handle The diaspora handle of the user liking the object
 * @param DOMDocument $raw_xml The raw XML recieved by the drupal system
 * @param object $account The drupal account object of the target user (if available)
 */
function hook_diaspora_like($target_type, $like_guid, $parent_guid, $author_handle, $raw_xml, $account = NULL) {
  
}

/**
 * A user is attempting to retract a like already sent.
 *
 * The like should be attempted to be deleted and removed from the system.
 *
 * @param string $target_type A String description of the type of object being liked (Post/Comment)
 * @param string $like_guid The GUID of the like
 * @param string $parent_guid The GUID of the object being liked
 * @param string @author_handle The diaspora handle of the user liking the object
 * @param DOMDocument $raw_xml The raw XML recieved by the drupal system
 * @param object $account The drupal account object of the target user (if available)
 */
function hook_diaspora_like_retraction($target_type, $like_guid, $parent_guid, $author_handle, $raw_xml, $account = NULL) {
  
}