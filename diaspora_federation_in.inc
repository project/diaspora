<?php

include_once('diaspora_utils.inc');

function get_between($input, $start, $end) 
{ 
  $substr = substr($input, strlen($start)+strpos($input, $start), (strlen($input) - strpos($input, $end, strpos($input, $start)))*(-1)); 
  return $substr;
}


function diaspora_public_receive() {
  $message = urldecode(urldecode($_REQUEST['xml']));
  
  $message_log = array();
  $message_log['raw_message'] = $message;
  $message_log['target_guid'] = 0;
  drupal_write_record('diaspora_message_log', $message_log);
  
  $base64url_encoded_prepared_payload_message = trim(get_between($message, "<me:data type='application/xml'>" , '</me:data>'));
  
  $message_log['decrypted_message'] = $base64url_encoded_prepared_payload_message;
  drupal_write_record('diaspora_message_log', $message_log, 'mid');  
  
  $handle = get_between($message,'<author_id>', '</author_id>');
  $source_user = user_load_by_name($handle);
  watchdog('diaspora', $message, WATCHDOG_DEBUG);
  $sig = get_between($message,'<me:sig>', '</me:sig>');
  
  if (!$source_user) {
    $source_user = _diaspora_user_save($handle);
  }
  
  if ($source_user) {
    $select = db_select('diaspora_external_user');
    $select->fields('diaspora_external_user');
    $select->condition('uid', '' . $source_user->uid);
    $external_diaspora_user = $select->execute()->fetchAll();
    if ($external_diaspora_user) {
      $external_diaspora_user = $external_diaspora_user[0];
      
      $base64 = strtr($base64url_encoded_prepared_payload_message, '-_', '+/');
      $plainText = trim(base64_decode($base64));
      watchdog('diaspora', $plainText, WATCHDOG_DEBUG);
      
      
      
      diaspora_handle_message($plainText);
      
    } else {
      watchdog('diaspora', 'Unable to process public message from: ' . $handle , WATCHDOG_ERROR);
    }
  } else {
     watchdog('diaspora', 'Unable to process public message from: ' . $handle , WATCHDOG_ERROR);
  }
}

function diaspora_private_receive2($guid) {
  diaspora_private_receive($guid);
}

function diaspora_private_receive($guid) {
  $message = urldecode(urldecode($_REQUEST['xml']));
  
  $message_log = array();
  $message_log['raw_message'] = $message;
  $message_log['target_guid'] = $guid;
  drupal_write_record('diaspora_message_log', $message_log);
  
  $select = db_select('diaspora_user');
  $select->fields('diaspora_user');
  $select->condition('uuid', $guid);
  $diaspora_user = $select->execute()->fetchAll();
  if ($diaspora_user) {
    $diaspora_user = $diaspora_user[0];
  } else {
    drupal_not_found();
    return;
  }
 
  watchdog('diaspora', $message, WATCHDOG_DEBUG);
  $encrypted_header_json_object = json_decode(base64_decode(trim(get_between($message, '<encrypted_header>' , '</encrypted_header>'))), TRUE);
  $base64_encoded_encrypted_outer_aes_key_bundle = $encrypted_header_json_object['aes_key'];
  $priv_key_resource = openssl_pkey_get_private($diaspora_user->private_key);
  $json_outer_aes_key_bundle = '';
  $result = openssl_private_decrypt(base64_decode($base64_encoded_encrypted_outer_aes_key_bundle), $json_outer_aes_key_bundle, $priv_key_resource);
  $outer_aes_key_bundle = json_decode($json_outer_aes_key_bundle, TRUE);
  $decrypted_header =  openssl_decrypt($encrypted_header_json_object['ciphertext'], 'aes-256-cbc', base64_decode($outer_aes_key_bundle['key']), false, base64_decode($outer_aes_key_bundle['iv']));
  $message_log['decrypted_header'] = $decrypted_header;
  drupal_write_record('diaspora_message_log', $message_log, 'mid');
  $base64url_encoded_prepared_payload_message = trim(get_between($message, "<me:data type='application/xml'>" , '</me:data>'));
  $inner_key = trim(get_between($decrypted_header, '<aes_key>' , '</aes_key>'));
  $inner_iv = trim(get_between($decrypted_header, '<iv>' , '</iv>'));
  $sender_handle = trim(get_between($decrypted_header, '<author_id>' , '</author_id>'));
  
  $prepared_payload_message = openssl_decrypt(diaspora_base64url_decode($base64url_encoded_prepared_payload_message), 'aes-256-cbc', base64_decode($inner_key), false, base64_decode($inner_iv));
  $message_log['decrypted_message'] = $prepared_payload_message;
  drupal_write_record('diaspora_message_log', $message_log, 'mid');  
  $base_string = $base64url_encoded_prepared_payload_message;
  $base_string .= ".YXBwbGljYXRpb24veG1s";
  $base_string .= ".YmFzZTY0dXJs";
  $base_string .= ".UlNBLVNIQTI1Ng==";

  module_load_include('inc', 'diaspora', 'diaspora_federation_out');

  $source_user_info  = _diaspora_external_user($sender_handle);
  $base64_public_key = strtr($source_user_info->public_key, '-_', '+/');
  $plain_text_public_key = base64_decode($base64_public_key);
    if (strpos($plain_text_public_key, '-----BEGIN RSA PUBLIC KEY-----') === 0) {
    $formatted_key = diaspora_openssh2pem($plain_text_public_key);
  } else {
    $formatted_key = $plain_text_public_key;
  }

  $public_key_res = openssl_pkey_get_public($formatted_key);
  
  $signature = diaspora_base64url_decode(trim(get_between($message, '<me:sig>' , '</me:sig>')));
  $verification = openssl_verify($base_string, $signature , $public_key_res, 'sha256');
  watchdog('diaspora', $verification ? 'Signature Passed' : 'Signature Failed', WATCHDOG_DEBUG);
  
  if ($verification) {
    watchdog('diaspora', $prepared_payload_message, WATCHDOG_DEBUG);
    diaspora_handle_message($prepared_payload_message, $diaspora_user->uid);
    drupal_add_http_header('Status', '202 Created');
    print 'Accepted';
  } else {
    watchdog('diaspora', 'Message failed signature verification', WATCHDOG_WARNING);
    drupal_add_http_header('Status', '400 Bad Request');
    print 'Bad Request';
  }
}

function diaspora_handle_message($message_string, $uid = NULL) {
  $xml = simplexml_load_string($message_string);
  $children = $xml->post->children();
  $message = $children[0];
  $message_type = $message->getName();
  
  switch($message_type) {
    case 'request' :
      handle_request($message, $uid);
      break;
    case 'retraction' :
      handle_retraction($message, $uid);
      break;
    case 'status_message':
      handle_status_message($message);
      break;
    case 'comment':
      handle_comment($message, $uid);
      break;
    case 'like':
      handle_like($message, $uid);
      break;
    case 'relayable_retraction':
    case 'signed_retraction':
      handle_relayable_retraction($message);
      break;
  }
}


/*
 
<XML>
  <post>
    <relayable_retraction>
      <parent_author_signature>ONdFbm6DfLxzu1mLJ1eTqMKVfq4zGfZQjKvPNIbKTr2EqkDQhYnlUeV1rMtUDlK+W4S44VDgGaBsRxJzUE2CBT7es7Ntzf5CB4qBdvI+bENupkDqqfwYWfzNp4UO22rnUZyctv7wP9JS+jkVeDCbnjedtG2Pw/Z4MJ+2fU7Gaklc3SO+mlUi0p0OX1J0wsVqFAalOr2YCmNai7bZwFF7vY5MmVXciOhsk3JV7r0Xw6OJ0IqwjL6D0JDCPdC5mJUtgtyA58H4sFzrtTHGIcaksm6+l/LejXGcLVaPZkkw6Kx4W8JuRC+d0S9Ybu6mFCmjS+Rfq14l89mji/0tDpDH3+4TxNyeIxWSXctFFDm9ABJPtKErW2KkdUbBjX8oVFRFNW1zeKmRUonhJ1ho02aXVcDnwlzGeYb7npe63Zqgzqv8dogT/wMNmQwvwrEMVUOBJbGXzcVAsOSWZeLLMFQXkkt3BjRCM0vOlTHS9pOCavyjzrLIRH2reDomwG+ycZSVvSx/Ywwc8ugKnCCGjJW9spnpjUncWdlgX10VlAboc1CF1jGB6Pryx9uDxsM5iB0YktlUPNKyFEfpYeDzjZAjwj7fCz9DY1BejkUXUpSEhZnB37IabMqK6PROC004S0t8dfzHipPTDyL/lhAJODZSHj49VjmcfBzQ44GPR+bZdZw=</parent_author_signature>
      <target_guid>baac8efa2156856b</target_guid>
      <target_type>Comment</target_type>
      <sender_handle>rurri_test@dia.rurri.com:3000</sender_handle>
      <target_author_signature>ONdFbm6DfLxzu1mLJ1eTqMKVfq4zGfZQjKvPNIbKTr2EqkDQhYnlUeV1rMtUDlK+W4S44VDgGaBsRxJzUE2CBT7es7Ntzf5CB4qBdvI+bENupkDqqfwYWfzNp4UO22rnUZyctv7wP9JS+jkVeDCbnjedtG2Pw/Z4MJ+2fU7Gaklc3SO+mlUi0p0OX1J0wsVqFAalOr2YCmNai7bZwFF7vY5MmVXciOhsk3JV7r0Xw6OJ0IqwjL6D0JDCPdC5mJUtgtyA58H4sFzrtTHGIcaksm6+l/LejXGcLVaPZkkw6Kx4W8JuRC+d0S9Ybu6mFCmjS+Rfq14l89mji/0tDpDH3+4TxNyeIxWSXctFFDm9ABJPtKErW2KkdUbBjX8oVFRFNW1zeKmRUonhJ1ho02aXVcDnwlzGeYb7npe63Zqgzqv8dogT/wMNmQwvwrEMVUOBJbGXzcVAsOSWZeLLMFQXkkt3BjRCM0vOlTHS9pOCavyjzrLIRH2reDomwG+ycZSVvSx/Ywwc8ugKnCCGjJW9spnpjUncWdlgX10VlAboc1CF1jGB6Pryx9uDxsM5iB0YktlUPNKyFEfpYeDzjZAjwj7fCz9DY1BejkUXUpSEhZnB37IabMqK6PROC004S0t8dfzHipPTDyL/lhAJODZSHj49VjmcfBzQ44GPR+bZdZw=</target_author_signature>
    </relayable_retraction>
  </post>
</XML>
*/
 
function handle_relayable_retraction($message) {
  
  if ('' . $message->target_type == 'Comment') {
    $select = db_select('field_data_diaspora_guid');
    $select->fields('field_data_diaspora_guid');
    $select->condition('diaspora_guid_value', '' . $message->target_guid);
    $select->condition('entity_type', 'comment');
    $comment_info = $select->execute()->fetchAll();
    if ($comment_info) {
      $comment_info = $comment_info[0];
      comment_delete($comment_info->entity_id);
    }
  }
  
  if ('' . $message->target_type == 'StatusMessage') {
    $select = db_select('field_data_diaspora_guid');
    $select->fields('field_data_diaspora_guid');
    $select->condition('diaspora_guid_value', '' . $message->target_guid);
    $select->condition('entity_type', 'node');
    $node_info = $select->execute()->fetchAll();
    if ($node_info) {
      $node_info = $node_info[0];
      node_delete($node_info->entity_id);
    }
  }
  
}

/*
<XML>
  <post>
    <comment>
      <guid>4df95c84a57b1e0a</guid>
      <parent_guid>ced4f4dd8534d350</parent_guid>
      <parent_author_signature>fneehjUYyYzhKkdrA6hSYXQib2v0SXYI5IhXztW9ULYJxwSo38ZDpraplRZjpfsC+C4oJYSMC/bMftyVsq3Kw93udnRqhn43vHCAtUtRZ0MX6N8f7NAXzJnnRPMchdejqQ6rrI3j3TEUiuGrH44lZaE7ytl6b1IQV26ysuwzRpm+j/H1MH3iBSzYsxfCiXGcI+lt9Ecxv1nWzvxGp6+iSuklQBW7rR/4tJSZHzYqvMuGl+ER/YbZicDhXo81DpqFuB1UexHAPtS+j+7gvMwL2wcu/F9HYoItQRoD7MFobeQPcot4AT2tmjATHPcZDP5qdRCkbYQAQFJdJ6pGHHD5HOcg8fZw4KQK0w9RJB/3XZdOggnXQOgPRF0a+Epp7uKRwyNP4wK57eahm/NYjhdVQOXc+ifuE+szh8UxwUDBGAWxrREqtXDBT7WwmtMK7xZvTBvvisP8030vOXIrJTizgltTFFaThj5wtsBkQJhUrAXW8QktTCsBTKOJZBgKitkWoGIokBNL8A6X/nInEUTPtP7yjRequky9RoXvI9gxE3tE7B/kLMNm0ZXC9iwKtktrxXpZhHpETKVVEufsPaAP8teGkV81FQYnXbjNz8aeGWOzpxJllVzB7YlM8yRH7JXbJS0ll83bZNlNhKVS8BGkG05+Mqt3jA03PO5I5LWyO9E=</parent_author_signature>
      <author_signature>fneehjUYyYzhKkdrA6hSYXQib2v0SXYI5IhXztW9ULYJxwSo38ZDpraplRZjpfsC+C4oJYSMC/bMftyVsq3Kw93udnRqhn43vHCAtUtRZ0MX6N8f7NAXzJnnRPMchdejqQ6rrI3j3TEUiuGrH44lZaE7ytl6b1IQV26ysuwzRpm+j/H1MH3iBSzYsxfCiXGcI+lt9Ecxv1nWzvxGp6+iSuklQBW7rR/4tJSZHzYqvMuGl+ER/YbZicDhXo81DpqFuB1UexHAPtS+j+7gvMwL2wcu/F9HYoItQRoD7MFobeQPcot4AT2tmjATHPcZDP5qdRCkbYQAQFJdJ6pGHHD5HOcg8fZw4KQK0w9RJB/3XZdOggnXQOgPRF0a+Epp7uKRwyNP4wK57eahm/NYjhdVQOXc+ifuE+szh8UxwUDBGAWxrREqtXDBT7WwmtMK7xZvTBvvisP8030vOXIrJTizgltTFFaThj5wtsBkQJhUrAXW8QktTCsBTKOJZBgKitkWoGIokBNL8A6X/nInEUTPtP7yjRequky9RoXvI9gxE3tE7B/kLMNm0ZXC9iwKtktrxXpZhHpETKVVEufsPaAP8teGkV81FQYnXbjNz8aeGWOzpxJllVzB7YlM8yRH7JXbJS0ll83bZNlNhKVS8BGkG05+Mqt3jA03PO5I5LWyO9E=</author_signature>
      <text>Comment 1</text>
      <diaspora_handle>rurri_test@dia.rurri.com:3000</diaspora_handle>
    </comment>
  </post>
</XML>
*/

function handle_comment($message, $uid = NULL) {
  
  $source_handle = '' . $message->diaspora_handle;
  $source_user = _diaspora_user_save($source_handle);
  $guid =  '' . $message->guid;
  $parent_guid =  '' . $message->parent_guid;
  $text =  '' . $message->text;
  $signature_encoded = '' . $message->author_signature;
  
  $account = NULL;
  if ($uid) {
    $account = user_load($uid);
  }
  
  /*Identify the following fields:
the guid of the comment
the guid of the original post that this is a comment to
the text of the comment
the diaspora handle of the author of the comment.*/
  
  $base_string = "$guid;$parent_guid;$text;$source_handle";
  

  $source_user_info  = _diaspora_external_user($source_handle);
  $base64_public_key = strtr($source_user_info->public_key, '-_', '+/');
  $plain_text_public_key = base64_decode($base64_public_key);
    if (strpos($plain_text_public_key, '-----BEGIN RSA PUBLIC KEY-----') === 0) {
    $formatted_key = diaspora_openssh2pem($plain_text_public_key);
  } else {
    $formatted_key = $plain_text_public_key;
  }

  $public_key_res = openssl_pkey_get_public($formatted_key);
  
  $signature = diaspora_base64url_decode($signature_encoded);
  $author_verifiction = openssl_verify($base_string, $signature , $public_key_res, 'sha256');
  watchdog('diaspora', $author_verifiction ? 'Comment Author Signature Passed' : 'Comment Author Signature Failed', WATCHDOG_DEBUG);
  
  if ($author_verifiction) {
    module_invoke_all('diaspora_comment', $guid, $parent_guid, $source_handle, $text, $message, $account);
  }
}

/* Example Message:
<XML>
  <post>
    <status_message>
      <raw_message>Test 19</raw_message>
      <guid>ced4f4dd8534d350</guid>
      <diaspora_handle>rurri_test@dia.rurri.com:3000</diaspora_handle>
      <public>true</public>
      <created_at>2012-02-23 17:37:13 UTC</created_at>
    </status_message>
  </post>
</XML>
*/

function handle_status_message($message, $uid = FALSE) {
  
  $guid = '' . $message->guid;
  $raw_message = '' . $message->raw_message;
  $author_handle = '' . $message->diaspora_handle;
  $created_at = strtotime('' . $message->created_at);
  $public = $message->public == 'true';
  if ($uid) {
    $target_account = user_load($uid);
  } else {
    $target_account = NULL;
  }
  
  module_invoke_all('diaspora_status_message', $raw_message, $guid, $author_handle, $created_at, $message, $public, $target_account);
  
}

/* Example Message:
  
<XML>
  <post>
    <retraction>
      <post_guid>78623893a75e7a46</post_guid>
      <diaspora_handle>rurri_test@dia.rurri.com:3000</diaspora_handle>
      <type>Person</type>
    </retraction>
  </post>
</XML>

*/

function handle_retraction($message, $uid) {
  $sender_handle = '' . $message->diaspora_handle;
  $sender_account = _external_diaspora_account($sender_handle);
  $local_account = user_load($uid);
  module_invoke_all('diaspora_unshare', $sender_account, $local_account, $sender_handle, $message);
}


/* Example Message:
  
<XML>
  <post>
    <request>
      <sender_handle>rurri_test@dia.rurri.com:3000</sender_handle>
      <recipient_handle>test3@home.rurri.com</recipient_handle>
    </request>
  </post>
</XML>

*/

function handle_request($message, $uid) {
  
  $links = discovery_discover($message->sender_handle);
  $result = drupal_http_request($links['http://microformats.org/profile/hcard']['href']);
  $sender_handle = '' . $message->sender_handle;
  $recipient_handle = '' . $message->recipient_handle;
  $external_user = _diaspora_user_save($sender_handle);
  $local_user = user_load($uid);
  
  module_invoke_all('diaspora_share', $external_user, $local_user, $sender_handle, $recipient_handle, $message);
}


/*
<XML>
  <post>
    <like>
      <guid>d7c03d6e48f4180d</guid>
      <target_type>Post</target_type>
      <parent_guid>4f4d7c7f51cc0</parent_guid>
      <author_signature>i7Rq+WLSidiFwJ3SHhBE4/+UgcTBIeFNKhgl5YgQrXiSA0QmQNj3dTivmBOb9o+L/XCY4rjGgv4o9G0FXhIOz58VSgE4juuIV1aE9nb3I8FcwCbUGsimdd04qVrlZhL0zNLT9juTXI5IE0rm6klznl7wV5oWjwGGMIw+1EehVPJ9gLzdhbesj9aCwVeADOcIzAiFaYNRXgyKeMhvuKDYa8lk53TXOXGCo0E8ponwN8VRo97SZTQzmvBXn/vefTPqNXedNoTlpq9prDOpiZipyH40g7mZ9w5KtDsMkbJsBye3P9Akk3pUFsWoVhW1oTgOpCxb0vwZxr6nVIbyEs5LWzEs88vLTqnEAOoDxljACSnjVTNcnRm/pXHkX6uIJphiK9j8U4nItBw2COoErlj0/CB8pD9NOrbCk7ous6dGd5sNuX7R958X+JKk3uzapPasfkNjN74oGYcXdcuMGobhiKUQYeT6fECKMVUzXo9BQgLX+Est3/P0v/DWUHPyF/JiqMqLvzwAnAOdO7nJMpWUjgbaEt6CPHvTo6/OP+7QyJ18SSO97EU7wmybn9Ju+mTOAL1EkYSo08R5JiXI/5rPC1mn5m3hDtm8aODpU/C2ZGfb3fp8urmaSzPaBtFEbADa85IXe6J9rwXmnVNqpLnVjW+HG5uAk8hr2EC7xP3aaMQ=</author_signature>
      <positive>true</positive>
      <diaspora_handle>rurri_test@dia.rurri.com:3000</diaspora_handle>
    </like>
  </post>
</XML>
*/

function handle_like($message, $uid = NULL) {
  
  $source_handle = '' . $message->diaspora_handle;
  $source_user = _diaspora_user_save($source_handle);
  $guid =  '' . $message->guid;
  $parent_guid =  '' . $message->parent_guid;
  $signature_encoded = '' . $message->author_signature;
  $target_type = '' . $message->target_type;
  $positive =  '' . $message->positive;
  
  $account = NULL;
  if ($uid) {
    $account = user_load($uid);
  }
  
  /*Identify the following fields:
the guid of the comment
the guid of the original post that this is a comment to
the text of the comment
the diaspora handle of the author of the comment.*/
  
  $base_string = "$guid;$target_type;$parent_guid;$positive;$source_handle";
  

  $source_user_info  = _diaspora_external_user($source_handle);
  $base64_public_key = strtr($source_user_info->public_key, '-_', '+/');
  $plain_text_public_key = base64_decode($base64_public_key);
    if (strpos($plain_text_public_key, '-----BEGIN RSA PUBLIC KEY-----') === 0) {
    $formatted_key = diaspora_openssh2pem($plain_text_public_key);
  } else {
    $formatted_key = $plain_text_public_key;
  }

  $public_key_res = openssl_pkey_get_public($formatted_key);
  
  $signature = diaspora_base64url_decode($signature_encoded);
  $author_verifiction = openssl_verify($base_string, $signature , $public_key_res, 'sha256');
  watchdog('diaspora', $author_verifiction ? 'Comment Author Signature Passed' : 'Comment Author Signature Failed', WATCHDOG_DEBUG);
  
  if ($author_verifiction) {
    if ($positive == 'true') {
      module_invoke_all('diaspora_like', $target_type, $guid, $parent_guid, $source_handle, $message, $account);
    } else {
      module_invoke_all('diaspora_like_retraction', $target_type, $guid, $parent_guid, $source_handle, $message, $account);
    }
  }
}
