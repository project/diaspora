<div id='content'>
<h1><?php print $account->name;?></h1>
<div id='content_inner'>
<div class='entity_profile vcard author' id='i'>
<h2>User profile</h2>
<dl class='entity_nickname'>
<dt>Nickname</dt>
<dd>
<a class='nickname url uid' href='<?php print url('diaspora/' , array('absolute' => TRUE));?>  rel='me'><?php print $account->name;?>@home.rurri.com</a>
</dd>
</dl>
<dl class='entity_given_name'>
<dt>First name</dt>
<dd>
<span class='given_name'><?php print $given_name;?></span>
</dd>
</dl>
<dl class='entity_family_name'>
<dt>Family name</dt>
<dd>
<span class='family_name'><?php print $family_name;?></span>
</dd>
</dl>
<dl class='entity_fn'>
<dt>Full name</dt>
<dd>
<span class='fn'><?php print $given_name;?> <?php print $family_name;?></span>
</dd>
</dl>
<dl class='entity_url'>
<dt>URL</dt>
<dd>
<a class='url' href='<?php print url('diaspora/' , array('absolute' => TRUE));?>' id='pod_location' rel='me'><?php print url('diaspora/' , array('absolute' => TRUE));?></a>
</dd>
</dl>
      <dl class='entity_photo'>
        <dt>Photo</dt>
        <dd>
          <img class='photo avatar' height='300px' src='<?php print image_style_url('diaspora_profile', ($account->picture->uri)); ?>'>
        </dd>
      </dl>
      <dl class='entity_photo_medium'>
        <dt>Photo</dt>
        <dd>
          <img class='photo avatar' height='100px' src='<?php print image_style_url('diaspora_medium_profile', ($account->picture->uri)); ?>'>
        </dd>
      </dl>
      <dl class='entity_photo_small'>
        <dt>Photo</dt>
        <dd>
          <img class='photo avatar' height='50px' src='<?php print image_style_url('diaspora_small_profile', ($account->picture->uri)); ?>'>
        </dd>
      </dl>
<dl class='entity_searchable'>
<dt>Searchable</dt>
<dd>
<span class='searchable'>true</span>
</dd>
</dl>
</div>
</div>
</div>