<?php




function _diaspora_user_save($handle) {
  $links = discovery_discover($handle);
  $result = drupal_http_request($links['http://microformats.org/profile/hcard']['href']);
  if (!$result->data) {
    watchdog('diaspora', 'Unable to save user: ' . $handle, WATCHDOG_WARNING);
    return FALSE;
  }
  $hcard = _diaspora_parse_hcard($result->data);
  $guid = $links['http://joindiaspora.com/guid']['href'];

  if (!$links || !$result || !$hcard || !$guid || sizeof($hcard) < 5) {
    return FALSE;
  }
  $account = NULL;
  $select = db_select('diaspora_external_user');
  $select->fields('diaspora_external_user');
  $select->condition('uuid', $guid);
  $existing_diaspora_user = $select->execute()->fetchAll();
  if ($existing_diaspora_user) {
    $existing_diaspora_user = (array)$existing_diaspora_user[0];
    $account = user_load($existing_diaspora_user['uid']);
  } else {
    $account = new stdClass();
    $diaspora_user = array();
  }

  
  foreach(_vcard_field_map() as $account_field => $hcard_field) {
    if (array_key_exists($hcard_field, $hcard)) {
      $account->{$account_field}[LANGUAGE_NONE][0]['value'] = $hcard[$hcard_field];
    }
  }
  $account->name = $handle;
  user_save($account);
  watchdog('diaspora', 'Saved:' . $guid, WATCHDOG_DEBUG);
  
  $diaspora_external_user = array();
  
  $diaspora_external_user['uuid'] = $guid;
  $diaspora_external_user['uid'] = $account->uid;
  $diaspora_external_user['account_uri'] = $handle;
  $diaspora_external_user['profile_location'] = $links['http://webfinger.net/rel/profile-page']['href'];
  $diaspora_external_user['seed_location'] = $links['http://joindiaspora.com/seed_location']['href'];
  $diaspora_external_user['hcard_location'] = $links['http://microformats.org/profile/hcard']['href'];
  $diaspora_external_user['public_user_feed_location'] = $links['http://schemas.google.com/g/2010#updates-from']['href'];
  $diaspora_external_user['public_key'] = $links['diaspora-public-key']['href'];
  $diaspora_external_user['firstname'] = $hcard['given_name'];
  $diaspora_external_user['lastname'] = $hcard['family_name'];
  $diaspora_external_user['nickname'] = $hcard['nickname'];
  $diaspora_external_user['avatar'] = $hcard['avatar'];
  $diaspora_external_user['avatar_medium'] = $hcard['avatar_medium'];
  $diaspora_external_user['avatar_small'] = $hcard['avatar_small'];
  $diaspora_external_user['searchable'] = ($hcard['searchable'] ? 1 : 0);
  
  //If user exists, do an update
  if ($existing_diaspora_user) {
    $return_value = db_update('diaspora_external_user')
        ->fields($diaspora_external_user)
        ->condition('uuid', $guid)
        ->condition('account_uri', $handle)
        ->execute();
  //Otherwise do an insert
  } else {
    $return_value = db_insert('diaspora_external_user')
        ->fields($diaspora_external_user)
        ->execute();
  }
  
  return $account;

}

function diaspora_pem2openssh($start_key) {
  $start_key = str_replace('-----BEGIN PUBLIC KEY-----', '', $start_key);
  $start_key = trim(str_replace('-----END PUBLIC KEY-----', '', $start_key));
  $start_key = str_replace("\n", '', $start_key);
  $key = substr(($start_key), 32);
  $key = "-----BEGIN RSA PUBLIC KEY-----\n" . wordwrap($key, 64, "\n", true) . "\n-----END RSA PUBLIC KEY-----";
  return $key;
}

function diaspora_openssh2pem($start_key) {
  $start_key = str_replace('-----BEGIN RSA PUBLIC KEY-----', '', $start_key);
  $start_key = trim(str_replace('-----END RSA PUBLIC KEY-----', '', $start_key));
  $key = 'MIICIjANBgkqhkiG9w0BAQEFAAOCAg8A' . str_replace("\n", '', $start_key);
  $key = "-----BEGIN PUBLIC KEY-----\n" . wordwrap($key, 64, "\n", true) . "\n-----END PUBLIC KEY-----";
  return $key;
}

function diaspora_base64url_decode($base64url) {
    $base64 = strtr($base64url, '-_', '+/');
    $plainText = base64_decode($base64);
    return ($plainText);
}


function diaspora_base64url_encode($data) { 
  return (strtr(base64_encode($data), '+/', '-_')); 
}

function diaspora_len($s) {
    $len = strlen($s);

    if ($len < 0x80) {
        return chr($len);
    }

    $data = dechex($len);
    $data = pack('H*', (strlen($data) & 1 ? '0' : '') . $data);
    return chr(strlen($data) | 0x80) . $data;
}

function _diaspora_external_user_by_uid($uid) {
  $select = db_select('diaspora_external_user');
  $select->fields('diaspora_external_user');
  $select->condition('uid', $uid);
  $diaspora_external_user = $select->execute()->fetchAll();
  if ($diaspora_external_user) {
    $diaspora_external_user = $diaspora_external_user[0];
    return $diaspora_external_user;
  } else {
    return FALSE;
  }
}

function _diaspora_user_by_uid($uid) {
  $select = db_select('diaspora_user');
  $select->fields('diaspora_user');
  $select->condition('uid', $uid);
  $diaspora_user = $select->execute()->fetchAll();
  if ($diaspora_user) {
    $diaspora_user = $diaspora_user[0];
    return $diaspora_user;
  } else {
    return FALSE;
  }
}


function _external_diaspora_account($handle) {
  $external_user = _diaspora_external_user($handle);
  if ($external_user) {
    $external_user = user_load($external_user->uid);
    return $external_user;
  } else {
    return FALSE;
  }
}

function _diaspora_external_user($handle) {
  $select = db_select('diaspora_external_user');
  $select->fields('diaspora_external_user');
  $select->condition('account_uri', $handle);
  $diaspora_external_user = $select->execute()->fetchAll();
  if ($diaspora_external_user) {
    $diaspora_external_user = $diaspora_external_user[0];
    return $diaspora_external_user;
  } else {
    $account = _diaspora_user_save($handle);
    if ($account) {
      $select = db_select('diaspora_external_user');
      $select->fields('diaspora_external_user');
      $select->condition('account_uri', $handle);
      $diaspora_external_user = $select->execute()->fetchAll();
      if ($diaspora_external_user) {
        $diaspora_external_user = $diaspora_external_user[0];
        return $diaspora_external_user;
      } else {
        return FALSE;
      }
    } else {
      return FALSE;
    }
  }
}

function _diaspora_parse_hcard($hcard_html) {
  $result = array();
  $dom = DOMDocument::loadHTML($hcard_html);
  
  $spans = $dom->getElementsByTagName('span');
  foreach($spans as $span) {
      foreach ( $span->attributes as $attribute )  {
        if ($attribute->name == 'class') {
          switch ($attribute->value) {
            case 'given_name' :
              $result['given_name'] = $span->textContent;
              break;
            case 'family_name' :
              $result['family_name'] = $span->textContent;
              break;
            case 'fn' :
              $result['fn'] = $span->textContent;
              break;
            case 'searchable' :
              $result['searchable'] = $span->textContent;
              break;
          }
        }
     }
  }
  
  $a_nodes = $dom->getElementsByTagName('a');
  foreach($a_nodes as $a_node) {
      foreach ( $a_node->attributes as $attribute )  {
        if ($attribute->name == 'class' && strpos($attribute->value, 'nickname') !== FALSE) {
          $result['nickname'] = $a_node->textContent;
        }
        if ($attribute->name == 'id' && $attribute->value='pod_location') {
          $result['pod_location'] = $a_node->textContent;
        }
     }
  }
  
  $img_nodes = $dom->getElementsByTagName('img');
  foreach($img_nodes as $img_node) {
    $correctImage = NULL;
    $imageSource = NULL;
    foreach ( $img_node->attributes as $attribute )  {
      if ($attribute->name == 'height' && $attribute->value == '300px') {
        $correctImage = 'avatar';
      }
      if ($attribute->name == 'height' && $attribute->value == '100px') {
        $correctImage = 'avatar_medium';
      }
      if ($attribute->name == 'height' && $attribute->value == '50px') {
        $correctImage = 'avatar_small';
      }
      if ($attribute->name == 'src') {
        $imageSource = $attribute->value;
      }
    }
    if ($correctImage) {
      $result[$correctImage] = $imageSource;
    }
  }
  
  return $result;
}

